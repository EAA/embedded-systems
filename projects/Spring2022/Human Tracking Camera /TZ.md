# Шаблон ТЗ по проекту


## Общие сведения

1. Цель проекта: реализовать систему интеллектуального отслеживания людей в помещении с использованием камеры видеонаблюдения и средств машинного обучения.
2. Команда исполнителей:
    - Граник Артем
    - Мардышкин Ростислав
    - Прен Павел
    - Чередников Егор

## Технические требования

1. Требования к функциональным характеристикам:
    - Система должна реализовывать обработку потокового видео и распознавание людей на видео
    - Система должна реализовывать возможность управления камерой через веб-приложение
2. Требования к надежности:
    - Безопасное соединение на уровняъ "камера - сервер" и "сервер - клиент"
3. Условия эксплуатации:
    - Функционирование при комнатной температуре
4. Требования к составу и параметрам технических средств:
    - Камера разрешением минимум 144р, желательно цветная
    - Средства, обеспечивающие соединение между камерой и сервером
5. Требования к информационной и программной совместимости:
    - Использование протоколов HTTP и RTSP для реализации обмена данными между камерой и сервером, сервером и клиентом веб-приложения
    - Реализация веб-приложения с архитектурой сервер-клиент
6. Требования к транспортированию и хранению:
    - Транспортировка камеры безопасным образом
    - Габариты камеры должны позволять транспортировать ее одному человеку

## Требования к документации

Программа будет состоять из нескольких составных частей - клиента и сервера:
1. Клиент - устройство, отвечающее за захват кадров с камеры, а затем отправление их на сервер в бесконечном цикле while
2. Сервер - компьютер, принимающий кадры от всех клиентов. При этом сервер должен детектировать объекты на входящих кадрах и хранить время  последней активности клиентов

## Технико-экономические показатели

Ориентировачная стоимость:
    - Камера видеонаблюдения: 2000 - 5000 рублей (бесплатно, если предоставит университет)
Итого: 2000 - 5000 рублей (бесплатно)
Ближайшим аналогом является система EYECONT для оценки скорости и характера движения людей (www.eyecont.ru)
## Cтадии и этапы разработки

Стадии разработки проекта:
    1. Постановка задачи проекта
    2. Формулирование и оформление технического задания проекта
    3. Подготовка проектной документации
    4. Подготовка материального обеспечения проекта
    5. Монтирование материальной части системы и разработка программного обеспечения
    6. Тестирование и отладка системы
    7. Подготовка презентации проекта
    8. Презентация и защита проекта

## Порядок контроля и приемки

В рамках испытаний результатов работы должны быть выполнены следующие требования:
    - Система должна распозновать человека на видео с камеры
    - Камера должна реагировать на команды, подаваемые ей через клиент веб-приложения
    - Система должна корректно отслеживать количество людей в помещении, где находится видео-камера

## Ссылки на источники

1. ГОСТ 19.201-78 Техническое задание. Требования к содержанию и оформлению
2. Сайт компании EYECONT. www.eyecont.ru
