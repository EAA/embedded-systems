//
//  CameraView.swift
//  CringeCar
//
//  Created by dokerplp on 3/10/22.
//

import SwiftUI

struct CameraView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CameraView_Previews: PreviewProvider {
    static var previews: some View {
        CameraView()
            .previewLayout(.fixed(width: 568, height: 320))
    }
}
